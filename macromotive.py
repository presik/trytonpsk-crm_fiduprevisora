# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields
from trytond.pyson import Eval


class Macromotive(ModelSQL, ModelView):
    "CRM Macromotive"
    __name__ = 'crm.macromotive'
    name = fields.Char('Name', required=True)
    general_reasons = fields.One2Many('crm.reason', 'macromotive', 'Reasons')
    description = fields.Text('Description', required=True)

    def get_rec_name(self, name):
        return self.name


class Reason(ModelSQL, ModelView):
    "CRM Reason"
    __name__ = 'crm.reason'
    macromotive = fields.Many2One('crm.macromotive', 'Macromotive')
    description = fields.Text('Name', required=True)
    parent = fields.Many2One('crm.reason', 'Parent')
    childs = fields.One2Many('crm.reason', 'parent', string='Children')
    types_reason = fields.One2Many('crm.type_reason', 'reason', 'Types Reason')

    def get_rec_name(self, name):
        return self.description

    @classmethod
    def view_attributes(cls):
        return super().view_attributes() + [
            ('/form/notebook/page[@id="childs"]', 'states',
                {'invisible': Eval('parent')}),
            ('/form/notebook/page[@id="type"]', 'states',
                {'invisible': ~Eval('parent')}),
        ]


class TypeReason(ModelSQL, ModelView):
    "CRM Type Reason"
    __name__ = 'crm.type_reason'
    reason = fields.Many2One('crm.reason', 'Reason')
    casual_details = fields.One2Many('crm.casual_detail', 'type_reason', 'Casual Details')
    description = fields.Text('Description', required=True)

    def get_rec_name(self, name):
        return self.description


class CasualDetail(ModelSQL, ModelView):
    "Casual Detail"
    __name__ = 'crm.casual_detail'
    type_reason = fields.Many2One('crm.type_reason', 'Type Reason')
    description = fields.Text('description')

    def get_rec_name(self, name):
        return self.description
