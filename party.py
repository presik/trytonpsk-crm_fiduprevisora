# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL, ModelView
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Bool, Not

AFILIATION_STATE = [
    ('', ''),
    ('retirado', 'Retirado'),
    ('activo', 'Activo'),
    ('proteccion_laboral', 'Protección Laboral'),
]


TYPE_DOCUMENT = {
    'Cédula de Ciudadanía': '13',
    'Registro Civil': '11',
    'Tarjeta de Identidad': '12',
    'Cédula de Extranjería': '22',
    'Salvoconducto': '47',
    'Certificado de Nacido Vivo': '10',
}


class RegionFiduprevisora(ModelSQL, ModelView):
    'Region Fiduprevisora'
    __name__ = 'crm.region_fiduprevisora'
    _rec_name = 'name'
    name = fields.Char('Name', required=True)
    departments = fields.Function(fields.One2Many('party.department_code', None,
        'Departments'), 'get_departments')

    def get_departments(self, name):
        Departments = Pool().get('party.department_code')
        departments = Departments.search([
            ('region', '=', self.id)
        ])
        if departments:
            return [d.id for d in departments]
        else:
            return []


class DepartmentCode(metaclass=PoolMeta):
    __name__ = "party.department_code"
    region = fields.Many2One('crm.region_fiduprevisora', 'Region')
    emails = fields.One2Many('crm.fiduprevisora_department.email',
        'department', 'Emails')


class DepartmentEmail(ModelSQL, ModelView):
    'Department - Emails'
    __name__ = 'crm.fiduprevisora_department.email'
    department = fields.Many2One('party.department_code', 'Department', required=True)
    email = fields.Char('Email')


class Party(metaclass=PoolMeta):
    __name__ = "party.party"
    is_affiliate_user = fields.Boolean('Is Affiliate User')
    city_attention = fields.Many2One('party.city_code', 'City Attention',
        states={
            'invisible': (Not(Bool(Eval('is_affiliate_user')))),
        })
    affiliation_state = fields.Selection(AFILIATION_STATE ,
        'Affiliation State', states={
            'invisible': (Not(Bool(Eval('is_affiliate_user')))),
        })

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls.type_document.selection.extend([('10', 'Certificado de Nacido Vivo')])

    @classmethod
    def import_data(cls, fields_names, data):
        fields_names += ['is_affiliate_user', 'name', 'type_person', 'id']
        idx_fields = {e: i for i, e in enumerate(fields_names)}
        dict_af_state = {v: k for k, v in AFILIATION_STATE}
        for r in data:
            id = ''
            id_number = r[idx_fields.get('id_number')]
            parties = cls.search(['id_number', '=', id_number])
            if parties:
                id = parties[0].id

            name = r[idx_fields.get('first_name')] + ' ' + r[idx_fields.get('second_name')]\
                + ' ' + r[idx_fields.get('first_family_name')] + ' ' + r[idx_fields.get('second_family_name')]
            name = name.strip()
            affiliation_state = dict_af_state.get(r[idx_fields.get('affiliation_state')])
            type_doc = TYPE_DOCUMENT.get(r[idx_fields.get('type_document')])
            is_affiliate_user = '0'
            if affiliation_state != 'retirado':
                is_affiliate_user = '1'
            type_person = 'persona_natural'
            r[idx_fields.get('affiliation_state')] = affiliation_state
            r[idx_fields.get('type_document')] = type_doc
            r += [is_affiliate_user, name, type_person, id]
        return super(Party, cls).import_data(fields_names, data)
