# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import configuration
from . import customer_service
from . import case
from . import party
from . import dash
from . import macromotive


def register():
    Pool.register(
        configuration.Configuration,
        case.Case,
        party.RegionFiduprevisora,
        party.DepartmentCode,
        party.Party,
        customer_service.ListSpecialty,
        customer_service.ReceiverService,
        customer_service.CustomerService,
        customer_service.ReceiverEmail,
        customer_service.FiduprevisoraAttachments,
        customer_service.FiduprevisoraReportStart,
        customer_service.HealthProvider,
        party.DepartmentEmail,
        customer_service.CustomerReceiver,
        customer_service.AppCustomerFiduprevisora,
        dash.DashApp,
        dash.AppConsultaPQR,
        macromotive.Macromotive,
        macromotive.Reason,
        macromotive.TypeReason,
        macromotive.CasualDetail,
        # customer_service.UserFiduprevisora,
        # customer_service.FiduprevisoraDepartment,
        # customer_service.FiduprevisoraCity,
        # customer_service.FiduprevisoraRegionDepartment,
        module='crm_fiduprevisora', type_='model')
    Pool.register(
        customer_service.FiduprevisoraReportWizard,
        module='crm_fiduprevisora', type_='wizard')
    Pool.register(
        customer_service.FiduprevisoraReport,
        module='crm_fiduprevisora', type_='report')
